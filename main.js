let obj = {
    a1: {
        b1: {
            c: 1
        },
        b2: {
            c: 222
        },
        b3: {
            c: {
                d: 33,
                e: 2.5,
                f: {
                    g: 9999,
                    h: {
                        i: {
                            j: 1001,
                            k: 'строка',
                            l: [1,2,3]
                        }
                    }
                }
            }
        }
    }
};

function generateObject(obj) {
    let newObj;
    if (Array.isArray(obj)) {
        newObj = [];
    } else
        newObj = {};
    for (let key in obj) {
        if (obj[key] !== null && typeof obj[key] === 'object') {
            newObj[key] = generateObject(obj[key]);
        } else newObj[key] = obj[key]
    } return newObj
}
let finalObject = generateObject(obj);
console.log(finalObject);